mod board;
use board::{State, Board};
use std::collections::HashSet;

pub fn try_queens(board: Board, states: &mut HashSet<Board>, depth: usize) -> usize {
    if depth < board.size {
        let mut sol_no = 0;
        for (new_queen, state) in &board {
            if state == State::Free {
                let mut mod_board = board.clone();
                mod_board.add_queen(new_queen);
                if !states.contains(&mod_board) {
                    sol_no += try_queens(mod_board, states, depth+1);
                }
            }
        }
        states.insert(board);
        sol_no
    } else if depth == board.size {
        states.insert(board);
        1
    } else {
        panic!("shouldn't have more than SIZE queens");
    }
}

pub fn run_size(size: usize) -> usize {
    let empty_board = Board::new(size);
    let mut states = HashSet::new();
    try_queens(empty_board, &mut states, 0)
}

#[test]
fn test() {
    let right_res = [1, 0, 0, 2, 10, 4, 40, 92, 352, 724, 2_680, 14_200,
        73_712, 365_596, 2_279_184, 14_772_512, 95_815_104, 666_090_624,
        4_968_057_848, 39_029_188_884, 314_666_222_712, 2_691_008_701_644,
        24_233_937_684_440, 227_514_171_973_736, 2_207_893_435_808_352,
        22_317_699_616_364_044, 234_907_967_154_122_528
    ];
    for i in 1..13 {
        let start = std::time::Instant::now();
        let my_res = run_size(i);
        println!("{} results for size {} in {:?}", my_res, i, start.elapsed());
        assert_eq!(my_res, right_res[i-1]);
    }
}
