use std::fmt;

#[derive(Clone, Hash, PartialEq, Eq)]
pub enum State {
    Occupied,
    Covered,
    Free,
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            State::Occupied => write!(f, "Q"),
            State::Covered => write!(f, "-"),
            State::Free => write!(f, "_"),
        }
    }
}

pub struct Pos {
    x: usize,
    y: usize,
}

impl Pos {
    pub fn new(x: usize, y: usize, size: usize) -> Pos {
        if x >= size || y >= size {
            panic!("position out of bounds");
        }
        Pos { x, y }
    }
}


impl fmt::Display for Pos {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct Board {
    pub size: usize,
    board: Vec<State>,
}

impl Board {
    pub fn new(size: usize) -> Self {
        Board {
            size,
            board: vec![State::Free; size * size],
        }
    }

    pub fn cover(&mut self, pos: &Pos) {
        match self[pos] {
         State::Free => self[pos] = State::Covered,
         State::Covered => {},
         State::Occupied => panic!("shouldn't have put queen"),
        }
    }

    pub fn add_queen(&mut self, new_queen: Pos) {
        if self[&new_queen] != State::Free {
            panic!("cannot put queen in non-free square");
        }
        self[&new_queen] = State::Occupied;

        for x in 0..self.size {
            if x != new_queen.x {
                self.cover(&Pos::new(x, new_queen.y, self.size))
            }
        }

        for y in 0..self.size {
            if y != new_queen.y {
                self.cover(&Pos::new(new_queen.x, y, self.size))
            }
        }

        for x in 0..self.size {
            // left to right diagonal
            let y_lr = new_queen.y as isize - (new_queen.x as isize - x as isize);
            // right to left diagonal
            let y_rl = new_queen.y as isize + (new_queen.x as isize - x as isize);

            for y in &[y_lr, y_rl] {
                if *y >= 0 && *y < self.size as isize {
                    let y = *y as usize;
                    if y != new_queen.y {
                        self.cover(&Pos::new(x, y, self.size));
                    }
                }
            }
        }
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut res = String::new();
        for (pos, state) in self {
            res.push_str(&state.to_string());
            if pos.y == self.size - 1 {
                res.push('\n');
            }
        }
        write!(f, "{}", res)
    }
}

impl std::ops::Index<&Pos> for Board {
    type Output = State;

    fn index(&self, pos: &Pos) -> &Self::Output {
        &self.board[pos.x * self.size + pos.y]
    }
}

impl std::ops::IndexMut<&Pos> for Board {
    fn index_mut(&mut self, pos: &Pos) -> &mut Self::Output {
        &mut self.board[pos.x * self.size + pos.y]
    }
}

pub struct BoardIntoIterator<'a> {
    board: &'a Board,
    index: usize,
}

impl Iterator for BoardIntoIterator<'_> {
    type Item = (Pos, State);

    fn next(&mut self) -> Option<Self::Item> {
        let size = self.board.size;
        if self.index < size * size {
            let pos = Pos::new(self.index / size, self.index % size, size);
            let state = self.board[&pos].clone();
            let res = (pos, state);
            self.index += 1;
            Some(res)
        } else {
            None
        }
    }
}

impl<'a> IntoIterator for &'a Board {
    type Item = (Pos, State);
    type IntoIter = BoardIntoIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        BoardIntoIterator {
            board: &self,
            index: 0,
        }
    }
}

